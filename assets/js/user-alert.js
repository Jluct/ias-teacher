function UserAlert(config) {
    var _self = this;
    _self.config = config;
}

/**
 * @param id
 * @param data
 *  text
 *  classHelper
 */
UserAlert.prototype.addAlert = function (id, data) {
    var alertTpl = $($('#user-alert').html());
    var content = $('<div class="alert-text-content"></div>');
    let textContent = '';
    if (data.text.constructor === Array) {
        for (let item in data.text) {
            textContent += data.text;
        }
    } else {
        textContent = data.text;
    }
    content = content.text(textContent);
    alertTpl.append(content);
    alertTpl.addClass(data.classHelper);

    $('.alerts-container').append(alertTpl);
};

window.alertModal = new UserAlert({});