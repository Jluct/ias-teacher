$(function () {
    $('#button-lesson-date').click(function () {
        $(this).addClass('d-none');
        $('#button-lesson-save').removeClass('d-none').addClass('.d-block');
        $('#lesson-date').removeClass('d-none').addClass('.d-block');
        $('.new-lesson-td input').removeClass('d-none').addClass('.d-block');
    });

    $('#button-lesson-save').click(function () {
        var note = [];
        $('.new-lesson-td input').each(function (indx, el) {
            let data = {
                "learnerStudyGroup": $(el).attr('data-lsg'),
                "note": 0 + $(el).prop('checked'),
                "updatedAt": $('#lesson-date').val(),
            };
            if ($(el).attr('data-id')) {
                data.id = $(el).attr('data-id');
            }
            note.push(data)
        });
        sendData(note, config.csrf, config.saveUrl, config.baseUrl);
    });

    $('.lesson-delete').click(function () {
        let el = $(this);
        let number = $('.lesson-delete').index(el);
        let tr = $('tbody tr');
        var note = [];
        for (let i = 0; i < tr.length; i++) {
            note.push($($(tr[i]).find('input')[number + 2]).attr('data-id'));
        }
        sendData(note, config.csrf, config.deleteUrl, config.baseUrl);
    });

    function sendData(data, csrf, url, baseUrl) {
        $.post(url, {"note": data, "csrf": csrf})
            .done(function (response) {
                if (baseUrl) {
                    window.location = baseUrl;
                }
            })
            .fail(function (response) {
                if (!response.errors) {
                    response.errors = [];
                }
                response.errors.unshift("Ошибка сохранения!");
                alertModal.addAlert(
                    'save-danger',
                    {
                        "text": response.errors,
                        "classHelper": "alert-danger"
                    }
                );
            })
            .always(function (response) {
                if (typeof response !== 'undefined' && response.csrf) {
                    config.csrf = response.csrf;
                }
            });
    }
});