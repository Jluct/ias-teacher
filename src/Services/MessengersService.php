<?php


namespace App\Services;

use App\Messengers\TelegramMessengers;
use App\Messengers\VkMessengers;

/**
 * Class MessengersService
 * @package App\Services
 */
class MessengersService
{
    /**
     * @var array MessengersInterface
     */
    private $messengers = [];

    /**
     * @var array
     */
    public $errors = [];

    /**
     * MessengersService constructor.
     * @param TelegramMessengers $telegramMessengers
     * @param VkMessengers $vkMessengers
     */
    public function __construct(TelegramMessengers $telegramMessengers, VkMessengers $vkMessengers)
    {
        $this->messengers['Telegram'] = $telegramMessengers;
        $this->messengers['VK'] = $vkMessengers;
    }

    /**
     * @param string $text
     * @return bool
     */
    public function send(string $text): bool
    {
        $isSuccess = true;
        foreach ($this->messengers as $name => $messenger) {
            if ($messenger->sendMessage($text)) {
                $isSuccess = false;
                $error = [
                    'messenger' => $name,
                    'message' => implode('; ', $messenger->getErrors())
                ];
                $this->errors[] = $error;
            }
        }

        return $isSuccess;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function ready(string $name): bool
    {
        return isset($this->messengers[$name]) ? $this->messengers[$name]->ready() : false;
    }
}