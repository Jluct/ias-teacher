<?php


namespace App\Interfaces;

/**
 * Interface MessengersInterface
 * @package App\Interfaces
 */
interface MessengersInterface
{
    /**
     * @param string $text
     * @return bool
     */
    public function sendMessage(string $text): bool;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @return bool
     */
    public function ready(): bool;
}