<?php

namespace App\Repository;

use App\Entity\Learner;
use App\Entity\LearnerStudyGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr;

/**
 * @method Learner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Learner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Learner[]    findAll()
 * @method Learner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LearnerRepository extends ServiceEntityRepository
{
    /**
     * LearnerRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Learner::class);
    }

    /**
     * @param int $group
     * @return LearnerStudyGroup[] Returns an array of LearnerStudyGroup objects
     */
    public function findByGroup(int $group)
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select('l')
            ->from(Learner::class, 'l')
            ->leftJoin('l.learnerStudyGroups', 'lsg', 'l.id = lsg.learner')
            ->where('lsg.studyGroup = :group')
            ->addOrderBy('l.name')
            ->setParameter('group', $group);

        try {
            return $builder->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        }

    }
}
