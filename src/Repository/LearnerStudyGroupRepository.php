<?php

namespace App\Repository;

use App\Entity\LearnerStudyGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LearnerStudyGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method LearnerStudyGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method LearnerStudyGroup[]    findAll()
 * @method LearnerStudyGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LearnerStudyGroupRepository extends ServiceEntityRepository
{
    /**
     * LearnerStudyGroupRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LearnerStudyGroup::class);
    }

    /*
    public function findOneBySomeField($value): ?LearnerStudyGroup
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
