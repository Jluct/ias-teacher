<?php

namespace App\Repository;

use App\Entity\StudentNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;

/**
 * @method StudentNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentNote[]    findAll()
 * @method StudentNote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentNoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentNote::class);
    }

    /**
     * @param int $group
     * @return array|null
     */
    public function findByGroup(int $group)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('sn', 'lsg', 'l')
            ->from(StudentNote::class, 'sn')
            ->leftJoin('sn.learnerStudyGroup', 'lsg', 'lsg.id=sn.learnerStudyGroup')
            ->leftJoin('lsg.learner', 'l', 'l.id=lsg.learner')
            ->where('lsg.studyGroup=:group')
            ->addOrderBy('sn.updatedAt')
            ->setParameter('group', $group);

        try {
            return $qb->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
