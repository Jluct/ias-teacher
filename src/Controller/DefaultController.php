<?php

namespace App\Controller;

use App\Entity\Learner;
use App\Entity\LearnerStudyGroup;
use App\Entity\Message;
use App\Entity\Messenger;
use App\Entity\StudentNote;
use App\Entity\StudyGroup;
use App\Form\MessageType;
use App\Form\NoteType;
use App\Messengers\VkMessengers;
use App\Services\MessengersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getGroups(PaginatorInterface $paginator, $page = 1)
    {
        $groupQuery = $this->getDoctrine()->getRepository(StudyGroup::class)
            ->getStudyGroupQuery();
        $pagination = $paginator->paginate(
            $groupQuery,
            $page,
            10
        );

        return $this->render('default/groups.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @param StudyGroup $group
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showGroup(StudyGroup $group)
    {
        $learners = $this->getDoctrine()->getRepository(Learner::class)
            ->findByGroup($group->getId());
        $note = $this->getDoctrine()->getRepository(StudentNote::class)
            ->findByGroup($group->getId());

        return $this->render('default/group.html.twig', [
            'group' => $group,
            'learners' => $learners,
            'note' => $note,
            'csrf' => $this->get('security.csrf.token_manager')->refreshToken('check-list')->getValue()
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function saveNoteGroup(Request $request)
    {
        $submittedToken = $request->request->get('csrf');
        $data = $request->get('note');
        $response = new JsonResponse();
        if (!$this->isCsrfTokenValid('check-list', $submittedToken)) {
            $response->setData(['errors' => ['csrf is not valid']]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return $response;
        }
        foreach ($data as $item) {
            if (!isset($item['id'])) {
                $note = $this->getDoctrine()->getRepository(StudentNote::class)
                    ->findOneBy([
                        'learnerStudyGroup' => $item['learnerStudyGroup'],
                        'updatedAt' => new \DateTime(!empty($item['updatedAt']) ? $item['updatedAt'] : date('Y-m-d'))
                    ]);
                if (empty($note)) {
                    $note = new StudentNote();
                }
            } else {
                $note = $this->getDoctrine()->getRepository(StudentNote::class)
                    ->find($item['id']);
            }
            $note->setNote($item['note']);
            $note->setUpdatedAt(new \DateTime(!empty($item['updatedAt']) ? $item['updatedAt'] : date('Y-m-d')));
            /** @var LearnerStudyGroup $learnerStudyGroup */
            $learnerStudyGroup = $this->getDoctrine()->getRepository(LearnerStudyGroup::class)
                ->find($item['learnerStudyGroup']);
            $note->setLearnerStudyGroup($learnerStudyGroup);
            $form = $this->createForm(NoteType::class, $note);
            if (!count($form->getErrors())) {
                $this->getDoctrine()->getManager()->persist($note);
            } else {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
        }
        try {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Данные сохранены');

            return $response;
        } catch (\Exception $e) {
            $response->setData(['errors' => ['Внутренняя ошибка сервера']]);
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $response->setData([
            'csrf' => $this->get('security.csrf.token_manager')->refreshToken('check-list')->getValue()
        ]);

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteNoteGroup(Request $request)
    {
        $submittedToken = $request->request->get('csrf');
        $data = $request->get('note');
        $response = new JsonResponse();
        if (!$this->isCsrfTokenValid('check-list', $submittedToken)) {
            $response->setData(['errors' => ['csrf is not valid']]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return $response;
        }
        $note = $this->getDoctrine()->getRepository(StudentNote::class)
            ->findBy(['id' => $data]);
        foreach ($note as $item) {
            $this->getDoctrine()->getManager()->remove($item);
        }
        $this->getDoctrine()->getManager()->flush();
        $response->setData([
            'csrf' => $this->get('security.csrf.token_manager')->refreshToken('check-list')->getValue()
        ]);

        return $response;
    }

    /**
     * @param MessengersService $messengersService
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param int $page
     * @return Response
     * @throws \Exception
     */
    public function messenger(MessengersService $messengersService, PaginatorInterface $paginator, Request $request, $page = 1)
    {
        $readyMessengers = [];
        $message = $this->getDoctrine()->getRepository(Message::class)
            ->findMessage();
        $pagination = $paginator->paginate($message, $page, 5);
        $messengers = $this->getDoctrine()->getRepository(Messenger::class)
            ->findAll();
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        $message->setCreatedAt(new \DateTime());
        for ($i = 0; $i < count($messengers); $i++) {
            $readyMessengers[$messengers[$i]->getName()] = $messengersService->ready($messengers[$i]->getName());
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $messengersService->send($message->getText());
            VarDumper::dump($messengersService->errors);
            $this->getDoctrine()->getManager()->persist($message);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Данные сохранены');
        }

        return $this->render('default/messengers.html.twig', [
            'pagination' => $pagination,
            'messengers' => $messengers,
            'readyMessengers' => $readyMessengers,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param VkMessengers $vkMessengers
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \VK\Exceptions\VKClientException
     * @throws \VK\Exceptions\VKOAuthException
     */
    public function vkMessengerAuth(VkMessengers $vkMessengers, Request $request)
    {
        $redirectUri = $this->generateUrl('vk_auth', [], UrlGeneratorInterface::ABSOLUTE_URL);
        if (!$request->get('code')) {
            $url = $vkMessengers->authUser($redirectUri);
            if ($url) {
                return $this->redirect($vkMessengers->authUser($redirectUri));
            } else {
                $this->addFlash('warning', 'Вы уже авторизованны');
            }
        } else {
            $vkMessengers->authGroup($request->get('code'), $redirectUri);
        }

        return $this->redirectToRoute('messenger');
    }
}
