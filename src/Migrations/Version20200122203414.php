<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200122203414 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE learner (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, contract TINYINT(1) DEFAULT NULL, payment TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE learner_study_group (learner_id INT NOT NULL, study_group_id INT NOT NULL, INDEX IDX_94B0F51D6209CB66 (learner_id), INDEX IDX_94B0F51D5DDDCCCE (study_group_id), PRIMARY KEY(learner_id, study_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE learner_study_group ADD CONSTRAINT FK_94B0F51D6209CB66 FOREIGN KEY (learner_id) REFERENCES learner (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE learner_study_group ADD CONSTRAINT FK_94B0F51D5DDDCCCE FOREIGN KEY (study_group_id) REFERENCES study_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE learner_study_group DROP FOREIGN KEY FK_94B0F51D6209CB66');
        $this->addSql('DROP TABLE learner');
        $this->addSql('DROP TABLE learner_study_group');
    }
}
