<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messenger", mappedBy="messages")
     */
    private $messengers;

    public function __construct()
    {
        $this->messengers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Messenger[]
     */
    public function getMessengers(): Collection
    {
        return $this->messengers;
    }

    public function addMessenger(Messenger $messenger): self
    {
        if (!$this->messengers->contains($messenger)) {
            $this->messengers[] = $messenger;
            $messenger->setMessages($this);
        }

        return $this;
    }

    public function removeMessenger(Messenger $messenger): self
    {
        if ($this->messengers->contains($messenger)) {
            $this->messengers->removeElement($messenger);
            // set the owning side to null (unless already changed)
            if ($messenger->getMessages() === $this) {
                $messenger->setMessages(null);
            }
        }

        return $this;
    }
}
