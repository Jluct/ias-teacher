<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LearnerStudyGroupRepository")
 */
class LearnerStudyGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Learner", inversedBy="learnerStudyGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $learner;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StudyGroup", inversedBy="learnerStudyGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $studyGroup;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StudentNote", mappedBy="learnerStudyGroup")
     */
    private $studentNotes;

    public function __construct()
    {
        $this->studentNotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLearner(): ?Learner
    {
        return $this->learner;
    }

    public function setLearner(?Learner $learner): self
    {
        $this->learner = $learner;

        return $this;
    }

    public function getStudyGroup(): ?StudyGroup
    {
        return $this->studyGroup;
    }

    public function setStudyGroup(?StudyGroup $studyGroup): self
    {
        $this->studyGroup = $studyGroup;

        return $this;
    }

    /**
     * @return Collection|StudentNote[]
     */
    public function getStudentNotes(): Collection
    {
        return $this->studentNotes;
    }

    public function addStudentNote(StudentNote $studentNote): self
    {
        if (!$this->studentNotes->contains($studentNote)) {
            $this->studentNotes[] = $studentNote;
            $studentNote->setLearnerStudyGroup($this);
        }

        return $this;
    }

    public function removeStudentNote(StudentNote $studentNote): self
    {
        if ($this->studentNotes->contains($studentNote)) {
            $this->studentNotes->removeElement($studentNote);
            // set the owning side to null (unless already changed)
            if ($studentNote->getLearnerStudyGroup() === $this) {
                $studentNote->setLearnerStudyGroup(null);
            }
        }

        return $this;
    }
}
