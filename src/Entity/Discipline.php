<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DisciplineRepository")
 */
class Discipline
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $hoursNumber;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\StudyGroup", mappedBy="discipline")
     */
    private $studyGroups;

    public function __construct()
    {
        $this->studyGroups = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Discipline
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHoursNumber(): ?int
    {
        return $this->hoursNumber;
    }

    /**
     * @param int $hoursNumber
     * @return Discipline
     */
    public function setHoursNumber(int $hoursNumber): self
    {
        $this->hoursNumber = $hoursNumber;

        return $this;
    }

    /**
     * @param ArrayCollection $studyGroups
     */
    public function setStudyGroups(ArrayCollection $studyGroups): void
    {
        $this->studyGroups = $studyGroups;
    }

    /**
     * @param StudyGroup $studyGroup
     */
    public function addStudyGroups(StudyGroup $studyGroup): void
    {
        $this->studyGroups->add($studyGroup);
    }

    /**
     * @return ArrayCollection
     */
    public function getStudyGroups(): ArrayCollection
    {
        return $this->studyGroups;
    }
}
