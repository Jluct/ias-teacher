<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LearnerRepository")
 */
class Learner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $contract;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $payment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LearnerStudyGroup", mappedBy="learner", orphanRemoval=true)
     */
    private $learnerStudyGroups;

    public function __construct()
    {
        $this->learnerStudyGroups = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Learner
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getContract(): ?bool
    {
        return $this->contract;
    }

    /**
     * @param bool|null $contract
     * @return Learner
     */
    public function setContract(?bool $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPayment(): ?bool
    {
        return $this->payment;
    }

    /**
     * @param bool|null $payment
     * @return Learner
     */
    public function setPayment(?bool $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return Collection|LearnerStudyGroup[]
     */
    public function getLearnerStudyGroups(): Collection
    {
        return $this->learnerStudyGroups;
    }

    /**
     * @param LearnerStudyGroup $learnerStudyGroup
     * @return Learner
     */
    public function addLearnerStudyGroup(LearnerStudyGroup $learnerStudyGroup): self
    {
        if (!$this->learnerStudyGroups->contains($learnerStudyGroup)) {
            $this->learnerStudyGroups[] = $learnerStudyGroup;
            $learnerStudyGroup->setLearner($this);
        }

        return $this;
    }

    /**
     * @param LearnerStudyGroup $learnerStudyGroup
     * @return Learner
     */
    public function removeLearnerStudyGroup(LearnerStudyGroup $learnerStudyGroup): self
    {
        if ($this->learnerStudyGroups->contains($learnerStudyGroup)) {
            $this->learnerStudyGroups->removeElement($learnerStudyGroup);
            // set the owning side to null (unless already changed)
            if ($learnerStudyGroup->getLearner() === $this) {
                $learnerStudyGroup->setLearner(null);
            }
        }

        return $this;
    }
}
