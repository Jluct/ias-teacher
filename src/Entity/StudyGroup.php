<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudyGroupRepository")
 */
class StudyGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Teacher
     * @ORM\ManyToOne(targetEntity="App\Entity\Teacher", inversedBy="studyGroups")
     */
    private $teacher;

    /**
     * @var Discipline
     * @ORM\ManyToOne(targetEntity="App\Entity\Discipline", inversedBy="studyGroups")
     */
    private $discipline;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LearnerStudyGroup", mappedBy="studyGroup", orphanRemoval=true)
     */
    private $learnerStudyGroups;

    /**
     * StudyGroup constructor.
     */
    public function __construct()
    {
        $this->learnerStudyGroups = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return StudyGroup
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Teacher $teacher
     */
    public function setTeacher(Teacher $teacher): void
    {
        $this->teacher = $teacher;
        $this->teacher->addStudyGroups($this);
    }

    /**
     * @return Teacher
     */
    public function getTeacher(): Teacher
    {
        return $this->teacher;
    }

    /**
     * @return Discipline
     */
    public function getDiscipline(): Discipline
    {
        return $this->discipline;
    }

    /**
     * @param Discipline $discipline
     */
    public function setDiscipline(Discipline $discipline): void
    {
        $this->discipline = $discipline;
    }

    /**
     * @return Collection|LearnerStudyGroup[]
     */
    public function getLearnerStudyGroups(): Collection
    {
        return $this->learnerStudyGroups;
    }

    /**
     * @param LearnerStudyGroup $learnerStudyGroup
     * @return StudyGroup
     */
    public function addLearnerStudyGroup(LearnerStudyGroup $learnerStudyGroup): self
    {
        if (!$this->learnerStudyGroups->contains($learnerStudyGroup)) {
            $this->learnerStudyGroups[] = $learnerStudyGroup;
            $learnerStudyGroup->setStudyGroup($this);
        }

        return $this;
    }

    /**
     * @param LearnerStudyGroup $learnerStudyGroup
     * @return StudyGroup
     */
    public function removeLearnerStudyGroup(LearnerStudyGroup $learnerStudyGroup): self
    {
        if ($this->learnerStudyGroups->contains($learnerStudyGroup)) {
            $this->learnerStudyGroups->removeElement($learnerStudyGroup);
            // set the owning side to null (unless already changed)
            if ($learnerStudyGroup->getStudyGroup() === $this) {
                $learnerStudyGroup->setStudyGroup(null);
            }
        }

        return $this;
    }
}
