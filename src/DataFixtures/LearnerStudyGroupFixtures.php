<?php

namespace App\DataFixtures;

use App\Entity\Learner;
use App\Entity\LearnerStudyGroup;
use App\Entity\StudyGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LearnerStudyGroupFixtures
 * @package App\DataFixtures
 */
class LearnerStudyGroupFixtures extends Fixture implements DependentFixtureInterface
{
    const LEARNER_STUDY_GROUP_NAME = 'learnerStudyGroup';

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < StudyGroupFixtures::STUDY_GROUP_COUNT; $i++) {
            for (
                $j = (int)(LearnerFixtures::LEARNER_COUNT / StudyGroupFixtures::STUDY_GROUP_COUNT) * $i;
                $j < (int)(LearnerFixtures::LEARNER_COUNT / StudyGroupFixtures::STUDY_GROUP_COUNT) * ($i + 1);
                $j++
            ) {
                $learnerStudyGroup = new LearnerStudyGroup();
                /** @var Learner $learner */
                $learner = $this->getReference(
                    sprintf(
                        '%s№%s',
                        LearnerFixtures::LEARNER_NAME,
                        $j
                    )
                );
                /** @var StudyGroup $studyGroup */
                $studyGroup = $this->getReference(
                    sprintf(
                        '%s №%s',
                        StudyGroupFixtures::STUDY_GROUP_NAME,
                        $i
                    )
                );

                $learnerStudyGroup->setLearner($learner);
                $learnerStudyGroup->setStudyGroup($studyGroup);
                $manager->persist($learnerStudyGroup);
                $this->addReference(sprintf('%s%s', self::LEARNER_STUDY_GROUP_NAME, $j), $learnerStudyGroup);
            }
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            StudyGroupFixtures::class,
            LearnerFixtures::class
        ];
    }
}
