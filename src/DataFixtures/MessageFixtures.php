<?php

namespace App\DataFixtures;

use App\Entity\Message;
use App\Entity\Messenger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class MessageFixtures
 * @package App\DataFixtures
 */
class MessageFixtures extends Fixture implements DependentFixtureInterface
{
    const MESSAGE_NAME = 'message';
    const MESSAGE_COUNT = 10;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::MESSAGE_COUNT; $i++) {
            $message = new Message();
            $message->setCreatedAt(new \DateTime());
            $message->setText('Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
            Consectetur corporis cum illo neque 
            nobis obcaecati quibusdam quidem, quisquam sed suscipit tempore voluptatibus. 
            Ducimus eligendi facere laudantium saepe sit unde veritatis.');
            for ($j = 0; $j < count(MessengerFixtures::$messengerData); $j++) {
                /** @var Messenger $messenger */
                $messenger = $this->getReference(MessengerFixtures::$messengerData[$j]['name']);
                $message->addMessenger($messenger);
            }
            $manager->persist($message);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            MessengerFixtures::class,
        ];
    }
}
