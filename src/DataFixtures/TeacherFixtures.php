<?php

namespace App\DataFixtures;

use App\Entity\Teacher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TeacherFixtures extends Fixture
{
    const TEACHER_NAME = 'teacher';
    const TEACHER_COUNT = 5;

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::TEACHER_COUNT; $i++) {
            $teacher = new Teacher();
            $name = sprintf('%s№%s', self::TEACHER_NAME, $i);
            $teacher->setName($name);
            $manager->persist($teacher);
            $this->addReference($name, $teacher);
        }

        $manager->flush();
    }
}
