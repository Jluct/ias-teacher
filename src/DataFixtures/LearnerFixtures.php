<?php

namespace App\DataFixtures;

use App\Entity\Learner;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LearnerFixtures
 * @package App\DataFixtures
 */
class LearnerFixtures extends Fixture
{
    const LEARNER_NAME = 'learner';
    const LEARNER_COUNT = 500;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::LEARNER_COUNT; $i++) {
            $learner = new Learner();
            $name = sprintf('%s№%s', self::LEARNER_NAME, $i);
            $learner->setName($name);
            $learner->setContract((bool)(($i + 1) % 2));
            $learner->setPayment((bool)(($i + 1) % 2));
            $manager->persist($learner);
            $this->addReference($name, $learner);
        }

        $manager->flush();
    }
}
