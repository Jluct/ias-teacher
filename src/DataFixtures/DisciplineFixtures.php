<?php

namespace App\DataFixtures;

use App\Entity\Discipline;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DisciplineFixtures
 * @package App\DataFixtures
 */
class DisciplineFixtures extends Fixture
{
    const DISCIPLINE_NAME = 'discipline';
    const DISCIPLINE_COUNT = 10;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::DISCIPLINE_COUNT; $i++) {
            $discipline = new Discipline();
            $discipline->setHoursNumber(40);
            $name = sprintf('%s№%s', self::DISCIPLINE_NAME, $i);
            $discipline->setName($name);
            $manager->persist($discipline);
            $this->addReference($name, $discipline);
        }

        $manager->flush();
    }
}
