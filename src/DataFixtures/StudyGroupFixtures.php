<?php

namespace App\DataFixtures;

use App\Entity\Discipline;
use App\Entity\Learner;
use App\Entity\StudyGroup;
use App\Entity\Teacher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class StudyGroupFixtures
 * @package App\DataFixtures
 */
class StudyGroupFixtures extends Fixture implements DependentFixtureInterface
{
    const STUDY_GROUP_NAME = 'group';
    const STUDY_GROUP_COUNT = 20;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::STUDY_GROUP_COUNT; $i++) {
            $group = new StudyGroup();
            $name = sprintf('%s №%s', self::STUDY_GROUP_NAME, $i);
            $group->setName($name);
            /** @var Teacher $teacher */
            $teacher = $this->getReference(
                sprintf(
                    '%s№%s',
                    TeacherFixtures::TEACHER_NAME,
                    (int)(TeacherFixtures::TEACHER_COUNT / self::STUDY_GROUP_COUNT * $i)
                )
            );
            $group->setTeacher($teacher);
            /** @var Discipline $discipline */
            $discipline = $this->getReference(
                sprintf(
                    '%s№%s',
                    DisciplineFixtures::DISCIPLINE_NAME,
                    (int)(DisciplineFixtures::DISCIPLINE_COUNT / self::STUDY_GROUP_COUNT * $i)
                )
            );
            $group->setDiscipline($discipline);

            $manager->persist($group);
            $this->addReference($name, $group);
        }
        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            TeacherFixtures::class,
            DisciplineFixtures::class,
            LearnerFixtures::class
        ];
    }
}
