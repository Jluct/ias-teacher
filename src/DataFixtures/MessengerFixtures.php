<?php

namespace App\DataFixtures;

use App\Entity\Messenger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class MessengerFixtures
 * @package App\DataFixtures
 */
class MessengerFixtures extends Fixture
{
    public static $messengerData = [
        [
            'name' => 'VK',
            'image' => '<i class="fa fa-vk"></i>'
        ],
        [
            'name' => 'Viber',
            'image' => '<i class="fab fa-viber"></i>'
        ],
        [
            'name' => 'Telegram',
            'image' => '<i class="fa fa-telegram"></i>'
        ],
        [
            'name' => 'WhatsApp',
            'image' => '<i class="fa fa-whatsapp"></i>'
        ]
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < count(self::$messengerData); $i++) {
            $messenger = new Messenger();
            $messenger->setName(self::$messengerData[$i]['name']);
            $messenger->setImage(self::$messengerData[$i]['image']);
            $manager->persist($messenger);
            $this->addReference(self::$messengerData[$i]['name'], $messenger);
        }

        $manager->flush();
    }
}
