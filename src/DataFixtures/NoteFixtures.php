<?php

namespace App\DataFixtures;

use App\Entity\LearnerStudyGroup;
use App\Entity\StudentNote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class NoteFixtures
 * @package App\DataFixtures
 */
class NoteFixtures extends Fixture implements DependentFixtureInterface
{
    const NOTE_DATE_SLICE = 3;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        for ($k = self::NOTE_DATE_SLICE; $k >= 0; $k--) {
            $date = new \DateTime();
            $date->modify(sprintf('-%s day', $k));
            for ($i = 0; $i < StudyGroupFixtures::STUDY_GROUP_COUNT; $i++) {
                for (
                    $j = (int)(LearnerFixtures::LEARNER_COUNT / StudyGroupFixtures::STUDY_GROUP_COUNT) * $i;
                    $j < (int)(LearnerFixtures::LEARNER_COUNT / StudyGroupFixtures::STUDY_GROUP_COUNT) * ($i + 1);
                    $j++
                ) {
                    /** @var LearnerStudyGroup $learnerStudyGroup */
                    $learnerStudyGroup = $this->getReference(sprintf('%s%s', LearnerStudyGroupFixtures::LEARNER_STUDY_GROUP_NAME, $j));
                    $note = new StudentNote();
                    $note->setUpdatedAt($date);
                    if ($j % 3 !== 0) {
                        $note->setNote(1);
                    } else {
                        $note->setNote(0);
                    }
                    $note->setLearnerStudyGroup($learnerStudyGroup);

                    $manager->persist($note);
                }
            }
            $manager->flush();
        }
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            LearnerStudyGroupFixtures::class,
        ];
    }
}
