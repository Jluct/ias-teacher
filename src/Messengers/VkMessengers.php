<?php


namespace App\Messengers;


use App\Interfaces\MessengersInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

/**
 * Class VkMessenger
 * @package App\Messengers
 */
class VkMessengers implements MessengersInterface
{
    /**
     * @var VKApiClient
     */
    private $botApi;

    /**
     * @var string
     */
    private $vkClientId;

    /**
     * @var string
     */
    private $vkAppId;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var bool
     */
    private $isReady = false;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * VkMessengers constructor.
     * @param string $vkClientId
     * @param string $vkAppId
     * @param SessionInterface $session
     */
    public function __construct(string $vkClientId, string $vkAppId, SessionInterface $session)
    {
        $this->botApi = new VKApiClient('5.5');
        $this->vkClientId = $vkClientId;
        $this->vkAppId = $vkAppId;
        $this->session = $session;
        if ($this->session->get('accessToken')) {
            $this->accessToken = $this->session->get('accessToken');
            $this->isReady = true;
        }
    }

    /**
     * @param string $text
     * @return bool
     */
    public function sendMessage(string $text): bool
    {
        if (!$this->isReady) {
            $error = [
                'message' => 'Не авторизованы для VK'
            ];
            $this->errors[] = $error;

            return false;
        }
        try {
            $this->botApi->wall()->post(
                $this->accessToken,
                [
                    'message' => $text,
                    'from_group' => 1
                ]
            );
        } catch (\Exception $e) {
            $error = [
                'message' => $e->getMessage()
            ];
            $this->errors[] = $error;

            return false;
        }

        return true;

    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $redirectUri
     * @return string
     */
    public function authUser(string $redirectUri): string
    {
        if ($this->isReady) {
            $error = [
                'message' => 'Уже авторизован'
            ];
            $this->errors[] = $error;

            return false;
        }
        $oauth = new VKOAuth('5.7');
        $display = VKOAuthDisplay::PAGE;
        $scope = [VKOAuthUserScope::WALL, VKOAuthUserScope::GROUPS];
        $state = '12gj345tr0gvj6g4r';
        return $oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $this->vkClientId, $redirectUri, $display, $scope, $state);
    }

    /**
     * @param string $code
     * @param string $redirectUri
     * @throws \VK\Exceptions\VKClientException
     * @throws \VK\Exceptions\VKOAuthException
     */
    public function authGroup(string $code, string $redirectUri): void
    {
        if ($this->isReady) {
            $error = [
                'message' => 'Уже авторизован'
            ];
            $this->errors[] = $error;

            return;
        }
        $oauth = new VKOAuth('5.7');
        $response = $oauth->getAccessToken($this->vkClientId, $this->vkAppId, $redirectUri, $code);
        $this->accessToken = $response['access_token'];
        $this->session->set('accessToken', $this->accessToken);
        $this->isReady = true;
    }

    /**
     * @return bool
     */
    public function ready(): bool
    {
        return $this->isReady;
    }
}