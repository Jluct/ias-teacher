<?php


namespace App\Messengers;


use App\Interfaces\MessengersInterface;
use TelegramBot\Api\BotApi;

/**
 * Class TelegramMessengers
 * @package App\Messengers
 */
class TelegramMessengers implements MessengersInterface
{
    /**
     * @var MessengersInterface
     */
    private $botApi;

    /**
     * @var string
     */
    private $telegramChatId;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var bool
     */
    private $isReady = false;

    /**
     * TelegramMessengers constructor.
     * @param string $telegramToken
     * @param string $telegramChatId
     */
    public function __construct(string $telegramToken, string $telegramChatId)
    {
        $this->telegramChatId = $telegramChatId;
        $this->botApi = new BotApi($telegramToken);
        $this->isReady = true;
    }

    /**
     * @param string $text
     * @return bool
     * @throws \TelegramBot\Api\Exception
     * @throws \TelegramBot\Api\InvalidArgumentException
     */
    public function sendMessage(string $text): bool
    {
        try {
            $this->botApi->sendMessage($this->telegramChatId, $text);
        } catch (\Exception $e) {
            $error = [
                'message' => $e->getMessage()
            ];
            $this->errors[] = $error;

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function ready(): bool
    {
        return $this->isReady;
    }
}